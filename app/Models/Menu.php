<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillabel = ['nama','harga','deskripsi','category_id','foto'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
