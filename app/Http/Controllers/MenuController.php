<?php



namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Menu;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use File;
use App\Models\Cart;


class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function indexAdmin()
    {
        $menu = Menu::all();
        return view('menu.manage.crudMenu', compact('menu'));
    }

    public function indexAll()
    {
        $menu = Menu::all();
        return view('menu.kategori', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $category = Category::all();
    
        return view('menu.manage.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'category_id' => 'required',
            'deskripsi' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
        ],
        [
            'nama.required' => 'inputan nama harus diisi/tidak boleh kosong',
            'harga.required' => 'inputan harga harus diisi/tidak boleh kosong',
            'category_id.required' => 'inputan kategori harus diisi/tidak boleh kosong',
            'deskripsi.required' => 'inputan kandungan vitamin harus diisi/tidak boleh kosong',
            'foto.required' => 'inputan foto harus diisi/tidak boleh kosong',
        ]
        );

        $imageName = time().'.'.$request->foto->extension();

        $request->foto->move(public_path('gambar'), $imageName);


        $menu = new Menu;
        $menu->nama = $request->nama;
        $menu->harga = $request->harga;
        $menu->deskripsi = $request->deskripsi;
        $menu->category_id = $request->category_id;
        $menu->foto = $imageName;
        $menu->save();

        return redirect('/crudMenu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menu = Menu::find($id);

        return view('menu.show', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = DB::table('menu')->where('id', $id)->first();
        $category = Category::all();
        return view('menu.manage.edit', compact('menu','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'category_id' => 'required',
            'deskripsi' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
        ]);

        $menu = Menu::findOrFail($id);

        if($request->has('foto')){
            $path = "gambar/";
            File::delete($path . $menu->foto);
            $imageName = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('gambar'), $imageName);
            $menu->foto = $imageName;
        }

        $menu->nama = $request->nama;
        $menu->harga = $request->harga;
        $menu->deskripsi = $request->deskripsi;
        $menu->category_id = $request->category_id;
        
        $menu->save();

        return redirect('/crudMenu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::findorfail($id);
        $menu->delete();

        $path = "gambar/";
        File::delete($path . $menu->foto);

        return redirect('/crudMenu');
    }
}
