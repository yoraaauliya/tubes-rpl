<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class OwnerController extends Controller
{
    public function index()
    {   
        // $order = Order::where('status', 'settlement');
        $order = Order::all();
        $total = 0;
        foreach ($order as $cart) {
            $total += $cart->gross_amount;
        }

        return view('owner.laporan', compact('order', 'total'));
    }
}
