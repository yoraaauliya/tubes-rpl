<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Order;
class OrderController extends Controller
{
    public function index()
    {

        $carts = Cart::where('user_id', auth()->user()->id)
            ->join('cart_items', 'cart.id', '=', 'cart_items.cart_id')
            ->join('menu', 'cart_items.menu_id', '=', 'menu.id')
            ->get();
        
        $total = 0;
        foreach ($carts as $cart) {
            $total += $cart->harga * $cart->quantity;
        }

        return view('Pembeli.checkout', compact('carts', 'total'));
    }

    public function indexOrder(){

        $order = Order::all();       

        return view('menu.manage.order', compact('order'));
    }

    public function riwayat(){
        $order = Order::where('user_id', auth()->user()->id);

        return view('Pembeli.riwayat', compact('order'));
    }
}
