<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class UserController extends Controller
{
    public function store(Request $request)
    {

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request['password']);
        $user->role = 'customer';
        
        $user->save();      
        return redirect('/login');
    }

    public function index()
    {
        $admin = User::all();
        return view('owner.ManageAdmin', compact('admin'));
    }
}
