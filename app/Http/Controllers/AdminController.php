<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Models\User;

class AdminController extends Controller
{
    public function index()
    {
        $admin = User::all();
        return view('owner.ManageAdmin', compact('admin'));
    }

    public function create()
    {
        return view('owner.createAdmin');
    }

    public function store(Request $request)
    {

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request['password']);
        $user->role = 'admin';
        
        $user->save();      
        return redirect('/admin');
    }

    public function destroy($id)
    {
        $model = User::find($id);
        $model->delete();
        return redirect('admin');
    }

    
    
}
