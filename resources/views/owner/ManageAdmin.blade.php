<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partials.header')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container">
            <a href="/admin/create" class="btn btn-primary btn-sm my-2" style="border-radius: 9px" >Tambah Admin</a>
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                    <th scope="col">Action</th>
                    
                  </tr>
                </thead>
                <tbody>
                    @forelse ($admin as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->role}}</td>
                            
                            <td>
                                <form action="/admin/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <input type="submit" class="btn btn-sm btn-danger" value="Delete" style="border-radius: 9px">
                                </form>
                            </td>                                                   
                        </tr>                         
                    @empty
                        <tr>
                            <td>Data Admin Kosong</td>
                        </tr>
                    @endforelse  
                </tbody>
            
              </table>
      </div>
    </section>


</body>

@include('partials.script')
</html>
