<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partials.header')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container" style="margin-left: -12px">
        <div class="row" style="justify-content: center;">
            <div class="card ftco-animate" style="width: 480px; margin-top: 12px">
                <center>
                    <h4 class="card-title">Add Admin</h4>
                </center>
                <div class="row" style="justify-content: center; padding: 24px">
                   
                        <form action="/adminAdd" method="POST" enctype="multipart/form-data">
                            @csrf
                           
                            <div class="form-group">
                                <label >Nama Admin :</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>       
                    
                            <div class="form-group">
                                <label >Email:</label>
                                <input type="email" class="form-control" name="email" required>
                            </div>       
                             
                            <div class="form-group">
                                <label >Password :</label>
                                <input type="password" class="form-control" name="password">
                            </div>       
                                                                                      
                            
                            <button type="submit" class="btn btn-primary w-100" style="border-radius: 9px">Submit</button>
                        </form>
                    </div>
            </div>
        </div>
      </div>
    </section>


</body>

@include('partials.script')
</html>

