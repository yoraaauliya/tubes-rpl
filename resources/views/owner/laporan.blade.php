<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partials.header')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container">
            
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Status</th>
                    <th scope="col">Order Id</th>
                    <th scope="col">Total</th>
                    <th scope="col">Jenis Pembayaran</th>
                    <th scope="col">Waktu Pemesanan</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($order as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->status}}</td>
                            <td>{{$item->order_id}}</td>
                            <td>{{$item->gross_amount}}</td>
                            <td>{{$item->payment_type}}</td>
                            <td>{{$item->created_at}}</td>
                                
                                                                              
                        </tr>                         
                    @empty
                        <tr>
                            <td>Data Kategori Kosong</td>
                        </tr>
                    @endforelse  
                </tbody>
            
              </table>
              <p style="font-size: 20px; margin-left:780px">Total Pendapatan : IDR {{$total}}</p>
      </div>
    </section>


</body>

@include('partials.script')
</html>
