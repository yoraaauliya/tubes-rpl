<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('partials.header')
  </head>
  <body class="goto-here">
		@include('partials.contact')

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('partials.navbar')
	  </nav>
    <!-- END nav -->

    <section id="home-section" class="hero">
		  @include('partials.sidebar')
    </section>

    <section class="ftco-section">
			@include('partials.shipping')
		</section>

		<section class="ftco-section ftco-category ftco-no-pt">
			@include('partials.kategori')
		</section>

    <section class="ftco-section">
    	
    </section>
		
    <section class="ftco-section testimony-section">
      @include('partials.testimoni')
    </section>

    <hr>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  @include('partials.script')
    
  </body>
</html>