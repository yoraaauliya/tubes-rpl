
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="{{asset('template/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('template/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('template/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}">
  </head>

  <body class="goto-here">
    @include('partials.contact')

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('partials.navbar')
	</nav>

    <div class="hero-wrap hero-bread" style="background-image: url({{asset('template/images/bg_1.jpg')}});">
        <div class="container">
          <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Checkout</span></p>
              <h1 class="mb-0 bread">Checkout</h1>
            </div>
          </div>
        </div>
      </div>
  
      <section class="ftco-section">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-7 ftco-animate">
                          <form action="/payment" method="GET">
                              <h3 class="mb-4 billing-heading">Billing Details</h3>
                    <div class="row align-items-end">
                        <div class="col-md-6">
                      <div class="form-group">
                          <label for="firstname">Nama</label>
                        <input type="text" name="uname" class="form-control" placeholder="" required>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                          <label name="number" for="lastname">No HP</label>
                        <input type="number" name="number" class="form-control" placeholder="" required>
                      </div>
                  </div>
                  <div class="w-100"></div>
                      <div class="w-100"></div>
                      <div class="col-md-12">
                        <label for="streetaddress">Detail Alamat</label>
                          <div class="form-group">
                            
                            <textarea cols="83" rows="5" name="alamat" required></textarea>
                      </div>
                      </div>
                      <div class="col-md-6">                     
                      </div>
                      <input type="hidden" name="harga" readonly value="{{ $total }}" />
                      <input type="hidden" name="user" readonly value="{{ $userid }}" />
                      <input type="hidden" name="email" readonly value="" />
                  <div class="w-100"></div>
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary py-3 px-4">Payment</button>
                  </div>
                  </div>
                  
                </form>
                      </div>
                      <div class="col-xl-5">
                <div class="row mt-5 pt-3">
                    <div class="col-md-12 d-flex mb-5">
                        <div class="cart-detail cart-total p-3 p-md-4">
                            <h3 class="billing-heading mb-4">Cart Total</h3>
                            <p class="d-flex">
                                    <span>Subtotal</span>
                                    <span>{{ $total }}</span>
                                      
                                  </p>
                                  <p class="d-flex">
                                      <span>Delivery</span>
                                      <span>$0.00</span>
                                  </p>
                                  
                                  <hr>
                                  <p class="d-flex total-price">
                                      <span>Total</span>
                                      <span>IDR {{ $total }}</span>
                                  </p>
                                  </div>
                    </div>
                    
                    </div>
                </div>
            </div> <!-- .col-md-8 -->
          </div>
        </div>
      </section> <!-- .section -->
  
          



@include('partials.script')
    @if(session('alert-sucess'))
    <script>alert("{{session('alert-success')}}")</script>
    @elseif(session('alert-failed'))
    <script>alert("{{session('alert-failed')}}")</script>
    @endif


</body>
  
   
</html>