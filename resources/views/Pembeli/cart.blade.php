
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">
    
    <link rel="stylesheet" href="{{asset('template/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{asset('template/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('template/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{asset('template/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('template/css/style.css')}}">
  </head>

  <body class="goto-here">
    @include('partials.contact')

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('partials.navbar')
	</nav>

<div class="hero-wrap hero-bread" style="background-image: url({{asset('template/images/bg_1.jpg')}});">
  <div class="container">
    <div class="row no-gutters slider-text align-items-center justify-content-center">
      <div class="col-md-9 ftco-animate text-center">
          <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Cart</span></p>
        <h1 class="mb-0 bread">My Cart</h1>
      </div>
    </div>
  </div>
</div>

<section class="ftco-section ftco-cart">
        <div class="container">
            <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table">
                        <thead class="thead-primary">
                          <tr class="text-center">
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>Product name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                            @forelse ($carts as $item)
                                <tr class="text-center">
                                    <td class="product-remove">
                                        <form action="/delete-form-cart/{{ Auth::user()->id }}/{{ $item->id }}" method="post">
                                            @csrf
                                            @method('post')
    
                                            <button type="submit" class="btn btn-icon" style="width: 50px">
                                                <i class="ion-ios-close"></i>
                                            </button>
                                        </form>
                                    </td>
                                    
                                    <td class="image-prod"><div class="img" style="background-image:url({{ asset('gambar/' . $item->foto) }});"></div></td>
                                    
                                    <td class="product-name">
                                        <h3>{{ $item->nama }}</h3>
                                    </td>
                                    
                                    <td class="price">IDR {{ $item->harga }}</td>
                                    
                                    <td class="quantity">
                                        
                                        <div class="input-group mb-3">
                                            <form action="/add-quntity/{{ Auth::user()->id }}/{{ $item->id }}" method="post">
                                                @csrf
                                                @method('post')
        
                                                <button type="submit" class="btn btn-icon" style="width: 50px">
                                                    <i class="ion-ios-add"></i>
                                                </button>
                                            </form>
                                            <h5 class="btn btn-icon" style="width: 50px">{{ $item->quantity }}</h5>
                                            <form action="/subtract-quntity/{{ Auth::user()->id }}/{{ $item->id }}"
                                                method="post">
                                                @csrf
                                                @method('post')
        
                                                <button type="submit" class="btn btn-icon" style="width: 50px">
                                                    <i class="ion-ios-remove" ></i>
                                                </button>
                                            </form>
                                        </div>
                                </td>
                                    
                                    
                                </tr>

                        
                            
                        @empty
                            <a class="dropdown-item preview-item justify-content-center align-items-center"
                                style="width:10rem;">
                                <p>Nothing here</p>
                            </a>
                        @endforelse
                    </tbody>
                </table>
                                
                  </div>
                  
            </div>
            <p><a href="/checkout" class="btn btn-primary py-3 px-4" style="margin-top: 20px; margin-left:910px">Proceed to Checkout</a></p>
        </div>
        
    </section>

    
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


@include('partials.script')



</body>
  
   
</html>