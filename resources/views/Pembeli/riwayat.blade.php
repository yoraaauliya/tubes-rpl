<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partials.header')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container">
            
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Status</th>
                    <th scope="col">Order Id</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">No HP</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($order as $key=>$item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->status}}</td>
                            <td>{{$item->order_id}}</td>
                            <td>{{$item->uname}}</td>
                            <td>{{$item->alamat}}</td>
                            <td>{{$item->number}}</td>
                                
                                                                              
                        </tr>                         
                    @empty
                        <tr>
                            <td>Data Kosong</td>
                        </tr>
                    @endforelse  
                </tbody>
            
              </table>
      </div>
    </section>


</body>

@include('partials.script')
</html>
