<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    @include('partials.header')
    
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
    </nav>

    <section class="ftco-section">
    	<div class="container" style="margin-left: -12px">
        <div class="row" style="justify-content: center;">
            <div class="card ftco-animate" style="width: 480px; margin-top: 12px">
                <div class="row" style="justify-content: center; padding: 24px">
                    <h4 class="card-title">Edit Menu</h4>
                        <form action="/menu/{{$menu->id}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')
                            <div class="form-group">
                                <label >Nama Menu :</label>
                                <input type="text" class="form-control" value="{{$menu->nama}}" name="nama">
                            </div>       
                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Harga Menu :</label>
                                <input type="number" class="form-control" value="{{$menu->harga}}" name="harga">
                            </div>       
                            @error('harga')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >deskripsi :</label>
                                <input type="text" class="form-control" value="{{$menu->deskripsi}}" name="deskripsi">
                            </div>       
                            @error('deskripsi')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            {{-- <div class="form-group">
                                <label >Total Glukosa :</label>
                                <input type="text" class="form-control" value="{{$menu->totalGlukosa}}" name="totalGlukosa">
                            </div>       
                            @error('totalGlukosa')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Total Kalori :</label>
                                <input type="text" class="form-control" value="{{$menu->totalKalori}}" name="totalKalori">
                            </div>       
                            @error('totalKalori')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Total Karbohidrat :</label>
                                <input type="text" class="form-control" value="{{$menu->totalKarbohidrat}}" name="totalKarbohidrat">
                            </div>       
                            @error('totalKarbohidrat')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Total Protein :</label>
                                <input type="text" class="form-control" value="{{$menu->totalProtein}}" name="totalProtein">
                            </div>       
                            @error('totalProtein')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                    
                            <div class="form-group">
                                <label >Total Lemak :</label>
                                <input type="text" class="form-control" value="{{$menu->totalLemak}}" name="totalLemak">
                            </div>       
                            @error('totalLemak')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror --}}
                    
                            {{-- <div class="form-group">
                                <label >Kategori :</label>
                                <select name="kategori" class="form-control">
                                    <option value="{{$menu->kategori}}">{{$menu->kategori}}</option>
                                    <option value="Makanan">Makanan</option>
                                </select>
                            </div>  --}}
                            <div class="mb-3">
                                <label>Masukkan Kategori Produk</label>
                                <br>
                                <select name="category_id">
                                    <option disabled>-- Pilih Kategori Produk --</option>
                                    @foreach ($category as $item)
                                    @if ($item->id === $menu->category_id)
                                        <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                                    @else 
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>                            
                            <button type="submit" class="btn btn-primary w-100" style="border-radius: 9px">Submit</button>
                        </form>
                    </div>
            </div>
        </div>
      </div>
    </section>


</body>

@include('partials.script')
</html>

