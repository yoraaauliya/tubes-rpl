<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('partials.header')
  </head>
  <body class="goto-here">
    @include('partials.contact')

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('partials.navbar')
	</nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url({{asset('template/images/bg_1.jpg')}});">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">About us</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section ftco-no-pb ftco-no-pt bg-light">
			<div class="container">
				<div class="row">
					<div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{asset('template/images/about.jpg')}});">
					</div>
					<div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
	          <div class="heading-section-bold mb-4 mt-md-5">
	          	<div class="ml-md-0">
		            <h2 class="mb-4">Healthy You, Happy Me</h2>
	            </div>
	          </div>
	          <div class="pb-md-5">
	          	<p>Hari Gini Masih Belum Bisa Membiasakan Makanan Sehat, Coba Deh . Karena Kalau Kamu Memulai Hidup Sehat Dimulai Dengan Asupan Yang Sehat Itu Bisa Membuat Metabolisme Tubuhmu Sangat Bagus, Karena Kandungan Di Makan-Makanan Sehat Ini Banyak Mengandung Gizi Yang Sudah Bagus Untuk Dikonsumsi Utuk Tubuh. Jadi, Tunggu Apa Lagi!!! Yuk Mulai Hidup Sehat Dengan Memulai Memakan Makanan Yang Sehat</p>
							<p><a href="/DaftarMenu" class="btn btn-primary">Shop now</a></p>
						</div>
					</div>
				</div>
			</div>
		</section>

		
		
		<section class="ftco-section testimony-section">
            @include('partials.testimoni')
          </section>

          <section class="ftco-section">
			@include('partials.shipping')
		</section>

    

    
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  @include('partials.script')
    
    
  </body>
</html>