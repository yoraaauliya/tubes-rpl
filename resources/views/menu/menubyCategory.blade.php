{{-- <section class="ftco-section">
        <div class="container">
            <div class="row">
                @foreach ($menus as $item)
                    <div class="col-md-6 col-lg-3 ftco-animate">
                        <div class="product">
                           <a href="/menus/{{$item->id}}" class="img-prod"><img class="img-fluid" src="{{ asset('gambar/'.$item->foto) }}" >
                                <div class="overlay"></div>
                            </a>
                            <div class="text py-3 pb-4 px-3 text-center">
                                <h3><a href="#">{{ $item->nama }}</a></h3>
                                <div class="d-flex">
                                    <div class="pricing">
                                        <p class="price">Rp.{{ $item->harga }}</p>
                                    </div>
                                </div>
                                <div class="bottom-area d-flex px-3">
                                    <div class="m-auto d-flex">
                                       <form action="/add-to-cart/{{ Auth::user()->id }}/{{ $item->id }}" method="post">
                                           @csrf
                                           @method('post')
                                           <button class="btn btn-primary" type="submit">Add to cart</button>
                                       </form>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                @endforeach
            </div>
        </div>

   
</section> --}}



<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    @include('partials.header')
  </head>
  <body class="goto-here">
	@include('partials.contact')
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    @include('partials.navbar')
	  </nav>
    <!-- END nav -->

    <div class="hero-wrap hero-bread" style="background-image: url({{asset('template/images/bg_1.jpg')}});">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Products</span></p>
            <h1 class="mb-0 bread">Products</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center">
    			<div class="col-md-10 mb-5 text-center">
    				<ul class="product-category">
    					<li><a href="/DaftarMenu">All</a></li>
    					<li><a href="/menus/category/1" >Food</a></li>
    					<li><a href="/menus/category/2" class="active">Drink</a></li>
    					<li><a href="/menus/category/3" class="active">Snack</a></li>
    					<li><a href="/menus/category/4">Fruits</a></li>
    				</ul>
    			</div>
    		</div>
    		<div class="row">
				@foreach ($menus as $item)
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="product">
						   <a href="/menus/{{$item->id}}" class="img-prod"><img class="img-fluid" src="{{ asset('gambar/'.$item->foto) }}" >
								<div class="overlay"></div>
							</a>
							<div class="text py-3 pb-4 px-3 text-center">
								<h3><a href="#">{{ $item->nama }}</a></h3>
								<div class="d-flex">
									<div class="pricing">
										<p class="price">Rp.{{ $item->harga }}</p>
									</div>
								</div>
								<div class="bottom-area d-flex px-3">
									<div class="m-auto d-flex">
									   <form action="/add-to-cart/{{ Auth::user()->id }}/{{ $item->id }}" method="post">
										   @csrf
										   @method('post')
										   <button class="btn btn-primary" type="submit">Add to cart</button>
									   </form>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
        </div>
    	</div>
    
    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  @include('partials.script')
    
  </body>
</html>