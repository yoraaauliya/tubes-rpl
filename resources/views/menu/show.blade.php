
<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    @include('partials.header')
  </head>
  <body class="goto-here">
	@include('partials.contact')

    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
        @include('partials.navbar')
      </nav>


    <section class="ftco-section">
    	<div class="container" >
    		<div class="row" style="margin-left: 100px">
    			<div class="col-lg-5 mb-5 ftco-animate">
    				<a href="{{ asset('gambar/' . $menu->foto) }}" class="image-popup"><img src="{{ asset('gambar/' . $menu->foto) }}" class="img-fluid" alt="Colorlib Template"></a>
    			</div>
    			<div class="col-lg-6 product-details pl-md-5 ftco-animate">
    				<h3>{{ $menu->nama }}</h3>
    				<p class="price"><span>IDR {{ $menu->harga }}</span></p>
    				<p>{{ $menu->deskripsi }}</p>
						<div class="row mt-4">
							<div class="col-md-6">
								<div class="form-group d-flex">
		              <div class="select-wrap">
	                </div>
		            </div>
							</div>
							<div class="w-100"></div>
							<div class="input-group col-md-6 d-flex mb-3">
	          	</div>
	          	<div class="w-100"></div>
          	</div>
			  <form action="/add-to-cart/{{ Auth::user()->id }}/{{ $menu->id }}" method="post">
				@csrf
				@method('post')
				
				<input type="submit" class="btn btn-black py-3 px-5" value="Add To Cart">

				</form>
			 </div>
    			</div>
    		</div>
    	</div>
    </section>



    
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  @include('partials.script')

  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>
    
  </body>
</html>