<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6 order-md-last align-items-stretch d-flex">
                    <div class="category-wrap-2 ftco-animate img align-self-stretch d-flex" style="background-image:  url({{asset('template/images/category.jpg')}});">
                        <div class="text text-center">
                            <h2>Healty Food</h2>
                            <p>Protect the health of every home</p>
                            <p><a href="/DaftarMenu" class="btn btn-primary">Shop now</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end" style="background-image:  url({{asset('template/images/category-2.jpg')}});">
                        <div class="text px-3 py-1">
                            <h2 class="mb-0"><a>Fruits</a></h2>
                        </div>
                    </div>
                    <div class="category-wrap ftco-animate img d-flex align-items-end" style="background-image:  url({{asset('template/images/image_5.jpg')}});">
                        <div class="text px-3 py-1">
                            <h2 class="mb-0"><a>Food</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end" style="background-image:  url({{asset('template/images/category-3.jpg')}});">
                <div class="text px-3 py-1">
                    <h2 class="mb-0"><a>Drink</a></h2>
                </div>		
            </div>
            <div class="category-wrap ftco-animate img d-flex align-items-end" style="background-image:  url({{asset('template/images/snack.jpg')}});">
                <div class="text px-3 py-1">
                    <h2 class="mb-0"><a >Snack</a></h2>
                </div>
            </div>
        </div>
    </div>
</div>