
    <div class="container">
      <a class="navbar-brand" href="index.html">HealtyMe</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        
        <ul class="navbar-nav ml-auto">
          @guest
            <li class="nav-item active"><a href="/" class="nav-link">Home</a></li>
          @endguest
          @auth
            @if (Auth::user()->role != 'Owner') 
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Shop</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="/cart">Cart</a>
                  <a class="dropdown-item" href="/riwayat">Riwayat Order</a>
                </div>
              </li>
            @endif
         
            @if (Auth::user()->role == 'admin')
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Manage</a>
                <div class="dropdown-menu" aria-labelledby="dropdown04">
                  <a class="dropdown-item" href="/crudMenu">Menu</a>
                  <a class="dropdown-item" href="/manageOrder">Order</a>
                </div>
              </li>
            @endif

            @if (Auth::user()->role == 'Owner')
              <li class="nav-item"><a href="/laporan" class="nav-link">Data</a></li>
              <li class="nav-item"><a href="/admin" class="nav-link">Manage Admin</a></li>
            @endif
          @endauth
          @guest
            <li class="nav-item"><a href="/about" class="nav-link">About</a></li>
          @endguest
          @auth
            <li class="nav-item"><a href="/DaftarMenu" class="nav-link">Product</a></li>
            <li class="nav-item cta cta-colored"><a href="/cart" class="nav-link"><span class="icon-shopping_cart"></span></a></li>
            <li class="nav-item"><a  class="btn btn-primary " style="margin-top:12px" href="{{ route('logout') }}"
              onclick="event.preventDefault();document.getElementById('logout-form').submit();">
              {{ __('Logout') }}</a>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
            </li>
          @endauth
          @guest
            <li class="nav-item"><a href="/login" class="btn btn-primary " style="margin-top:12px" >Login</a></li>
          @endguest
          
      <ul>
      </div>
    </div>
 