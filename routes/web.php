

<?php
use App\Http\Controllers\WebController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\CartItemController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/about', function () {
    return view('menu.manage.about');
});


Route::resource('user', UserController::class);


Route::group(['middleware' => ['auth']], function () {

    Route::get('/payment', [WebController::class, 'payment']);
    Route::get('/checkout', [WebController::class, 'index']);

    Route::post('/payment', [WebController::class, 'payment_post']);

    Route::get('/home', [HomeController::class, 'index']);
    Route::resource('menu', MenuController::class);

    Route::get('/admin', [AdminController::class, 'index']);
    Route::get('/admin/create', [AdminController::class, 'create']);
    Route::post('/adminAdd', [AdminController::class, 'store']);
    Route::delete('/admin/{id}', [AdminController::class, 'destroy']);
    
    Route::get('/crudMenu', [MenuController::class, 'indexAdmin']);
    Route::get('/laporan', [OwnerController::class, 'index']);
    Route::get('/manageOrder', [OrderController::class, 'indexOrder']);
    Route::get('/riwayat', [OrderController::class, 'riwayat']);
    
    
    Route::get('/DaftarMenu', [MenuController::class, 'indexAll']);
    
    Route::get('/menus/category/{category_id}', [CategoryController::class, 'showProductByCategory']);
    Route::post('/add-to-cart/{user_id}/{menu_id}', [CartController::class, 'addToCart']);
    Route::post('/delete-form-cart/{user_id}/{menu_id}', [CartController::class, 'deleteCartItem']);
    Route::post('/subtract-quntity/{user_id}/{menu_id}', [CartController::class, 'subtractCartItemQuantity']);
    Route::post('/add-quntity/{user_id}/{menu_id}', [CartController::class, 'addCartItemQuantity']);
    
    
    Route::resource('cart', CartItemController::class);    
    
});

Auth::routes();


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
